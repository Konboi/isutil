package isutil

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"log"
	"time"

	"github.com/go-sql-driver/mysql"
	proxy "github.com/shogo82148/go-sql-proxy"
)

const (
	MySQLProxyName = "mysql-proxy"
)

func NewMySQLProxy() string {
	sql.Register(MySQLProxyName, proxy.NewProxyContext(&mysql.MySQLDriver{}, &proxy.HooksContext{
		PreExec: func(_ context.Context, _ *proxy.Stmt, _ []driver.NamedValue) (interface{}, error) {
			// The first return value(time.Now()) is passed to both `Hooks.Exec` and `Hook.ExecPost` callbacks.
			return time.Now(), nil
		},
		PostExec: func(_ context.Context, ctx interface{}, stmt *proxy.Stmt, args []driver.NamedValue, _ driver.Result, _ error) error {
			// The `ctx` parameter is the return value supplied from the `Hooks.PreExec` method, and may be nil.
			if stmt != nil {
				log.Printf("[isutil]\ttype:Exec\tquery:%s\targs:%v\ttime:%f\tsize:%d\n", stmt.QueryString, args, time.Since(ctx.(time.Time)).Seconds(), len(args))
			}
			return nil
		},
		PreQuery: func(_ context.Context, _ *proxy.Stmt, _ []driver.NamedValue) (interface{}, error) {
			// The first return value(time.Now()) is passed to both `Hooks.Query` and `Hook.QueryPost` callbacks.
			return time.Now(), nil
		},
		PostQuery: func(_ context.Context, ctx interface{}, stmt *proxy.Stmt, args []driver.NamedValue, _ driver.Rows, _ error) error {
			if stmt != nil {
				log.Printf("[isutil]\ttype:query\tquery:%s\targs:%v\ttime:%f\tsize:%d\n", stmt.QueryString, args, time.Since(ctx.(time.Time)).Seconds(), len(args))
			}
			return nil

		},
	}))

	return MySQLProxyName
}
